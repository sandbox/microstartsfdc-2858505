<?php
/**
 * @file
 * Stub file for microstart_menu_tree() and suggestion(s).
 */

/**
 * microstart theme wrapper function for the secondary menu links.
 */
function microstart_menu_tree__menu_anonymous(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_training(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_client(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_partner(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_volunteer(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_advisor(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_user_logged_off(&$variables) {
  return microstart_custom_menu_tree_logged_off($variables);
}

function microstart_menu_tree__menu_user_logged_in(&$variables) {
  return microstart_custom_menu_tree($variables);
}

function microstart_menu_tree__menu_wiki(&$variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

function microstart_custom_menu_tree(&$variables) {
  return '<ul class="menu nav navbar-nav secondary">' . $variables['tree'] . '</ul>';
}

function microstart_custom_menu_tree_logged_off(&$variables) {
//   $image = file_create_url('public://logo/sign_up.png');
//   $user_register = '<li><a href="/user/register"><img src="' . $image . '" style="width:48px;heigth:48px;"></a></li>';
//   return '<ul class="menu nav navbar-nav secondary">' . $user_register . $variables['tree'] . '</ul>';
  return microstart_custom_menu_tree($variables);
}

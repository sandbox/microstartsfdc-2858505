<?php
/**
 * @file
 * Stub file for microstart_menu_link() and suggestion(s).
 */

/**
 * Returns HTML for a menu link and submenu.
 *
 * @param array $variables
 *          An associative array containing:
 *          - element: Structured array data for a menu link.
 *
 * @return string The constructed HTML.
 *
 * @see theme_menu_link() @ingroup theme_functions
 */
function microstart_menu_link(array $variables) {
  $element = $variables ['element'];
  $output = l ( $element ['#title'], $element ['#href'], $element ['#localized_options'] );
  switch ($variables ['element'] ['#theme']) {
//     case 'menu_link__menu_user_logged_off' :
//       $image = file_create_url('public://logo/login.png');
//       return '<li' . drupal_attributes($element['#attributes']) . '><a href="/connection"><img src="' . $image . '"></a></li>';
//       break;
    case 'menu_link__menu_wiki' :
      return '<a class="btn btn-warning button-sharp background-color-secondary btn-lg" role="button" href="' . $GLOBALS ['base_url'] . '/node/add">' . $element ['#title'] . '</a>';
      break;
    default :
      return bootstrap_menu_link ( $variables );
  }
}


<?php //drupal_goto('/connection'); ?>
<div class="user-login">
<?php module_load_include('inc', 'user', 'user.pages'); ?>
<?php $form = drupal_get_form('user_login'); ?>
<?php print(drupal_render_children($form)); ?>
</div><!-- /.user-login -->
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    <?php $node = node_load(11); ?>
    <?php $index = 0; ?>
    <?php $fields = field_get_items('node', $node, 'field_log_in_text');?>
    <?php $output = field_view_value('node', $node, 'field_log_in_text', $fields[$index]); ?>
    $( "#edit-actions--2" ).replaceWith('<div><button type="submit" id="edit-submit--4" name="op" class="btn btn-primary button-sharp microstart-button-font-size" style="width:100%;">' + '<?php print render($output) ?>' + '</button></div>');
    });
})(jQuery);
</script>

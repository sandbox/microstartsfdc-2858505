<div class="user-profile-edit">
<div class="panel panel-default panel-microstart-back center-block">
<div class="panel-body">

<div class="row row-centered">
<div class="col-xs-12 col-sm-5 col-centered">
<div class="panel panel-default">
<div class="panel-body">

<?php module_load_include('inc', 'user', 'user.pages'); ?>
<?php $form = drupal_get_form('user_profile_form', user_load(arg(1))); ?>
<?php print(drupal_render_children($form)); ?>

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.user-profile-edit -->
<script type="text/javascript">
(function($) {
function displayOtherValue() {
  if ($('#edit-field-activity-sector-und-12--2').is(':checked')) {
    $('#edit-field-activity-sector-other--2').show();
    alert('orange');
  } else {
    $('#edit-field-activity-sector-other--2').hide();
  }
  if ($('#edit-field-competence-und-11--2').is(':checked')) {
    alert('orange');
    $('#edit-field-competence-other--2').show();
  } else {
    $('#edit-field-competence-other--2').hide();
  }
}
$('input:radio[name*=activity_sector]').on('change', function(){
  displayOtherValue();
});
$('input:checkbox[name*=competence]').on('change', function(){
  displayOtherValue();
});
$(document).ready(function() {
  $(".form-actions").insertAfter('#edit-timezone--3');
  $('label[for=edit-field-notification-event-und-none--2]').remove();
  $('label[for=edit-field-notification-coachee-und-none--2]').remove();
  $('label[for=edit-field-notification-wiki-und-none--2]').remove();
  $('#edit-mimemail--2').remove();
  $('#edit-timezone--3').remove();
  //$('#edit-locale--2').remove();
<?php
$circle_red = file_create_url('public://logo/circle_red.png');
$circle_green = file_create_url('public://logo/circle_green.png');
$output_circle_red = '<img src="' . $circle_red . '" style="width:20%;">';
$output_circle_green = '<img src="' . $circle_green . '" style="width:20%;">';
?>
  $("label[for='edit-field-disponibility-und-none--2']" ).remove();
  $('#edit-field-disponibility-und-0--2').after('<?php print $output_circle_red; ?>');
  $('#edit-field-disponibility-und-1--2').after('<?php print $output_circle_green; ?>');
  displayOtherValue();
});
})(jQuery);
</script>

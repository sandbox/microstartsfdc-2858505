<div class="user-pass">
<div class="panel panel-default panel-microstart-back center-block">
<div class="panel-body">

<div class="row row-centered">
<div class="col-xs-12 col-sm-5 col-centered">
<div class="panel panel-default">
<div class="panel-body">

<?php module_load_include('inc', 'user', 'user.pages'); ?>
<?php $form = drupal_get_form('user_pass'); ?>
<?php print(drupal_render_children($form)); ?>

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.user-pass -->

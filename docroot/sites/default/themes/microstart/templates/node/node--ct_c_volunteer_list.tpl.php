<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_volunteers_title']);
    hide($content['field_last_name_text']);
    hide($content['field_first_name_text']);
    hide($content['field_select_header']);
    hide($content['field_city_text']);
    hide($content['field_zip_text']);
    hide($content['field_disponibility_text']);
    hide($content['field_language_text']);
    hide($content['field_activity_sector_text']);
    hide($content['field_competence_text']);
    hide($content['field_all']);
    hide($content['field_no']);
    hide($content['field_yes']);
    hide($content['field_horeca']);
    hide($content['field_insurance']);
    hide($content['field_import_export']);
    hide($content['field_transport']);
    hide($content['field_cosmetic']);
    hide($content['field_art']);
    hide($content['field_personal_consultancy']);
    hide($content['field_small_shop']);
    hide($content['field_building']);
    hide($content['field_hairdressing']);
    hide($content['field_textile']);
    hide($content['field_other']);
    hide($content['field_business_model_development']);
    hide($content['field_financial_plan_expertise']);
    hide($content['field_startup_administration']);
    hide($content['field_daily_administration_busin']);
    hide($content['field_financial_followup']);
    hide($content['field_accountancy']);
    hide($content['field_marketing']);
    hide($content['field_communication']);
    hide($content['field_legal_expertise']);
    hide($content['field_human_resource']);
    hide($content['field_email_text']);
    hide($content['field_phone_text']);
    hide($content['field_mission_text']);
    hide($content['field_mission_project_planning']);
    hide($content['field_mission_startup']);
    hide($content['field_mission_expert']);
    hide($content['field_mission_coach']);
    hide($content['field_mission_animator']);
    hide($content['field_mission_former']);
    hide($content['field_mission_president']);
    hide($content['field_mission_recouvrement']);
    hide($content['field_mission_internal_com']);
    hide($content['field_mission_ambassador']);
    hide($content['field_mission_it_developer']);
    hide($content['field_mission_search_funding']);
    hide($content['field_mission_reception']);
    hide($content['field_mission_coach_of_coach']);
    hide($content['field_mission_administrator_awlp']);
    hide($content['field_photo_text']);
    hide($content['field_brussels_capital_region']);
    hide($content['field_walloon_brabant']);
    hide($content['field_flemish_brabant']);
    hide($content['field_antwerp']);
    hide($content['field_flemish_brabant_continued']);
    hide($content['field_limburg']);
    hide($content['field_liege']);
    hide($content['field_namur']);
    hide($content['field_hainaut']);
    hide($content['field_luxembourg']);
    hide($content['field_hainaut_continued']);
    hide($content['field_west_flanders']);
    hide($content['field_east_flanders']);
    hide($content['field_ms_pinpoint']);
    hide($content['field_ms_agency_address']);
  ?>
  <div class="ct_c_coaching_management">

<div class="panel panel-default panel-microstart-back center-block">
<div class="panel-body">

<div id="block_volunteer" class="row row-centered" style="padding-bottom:10px;">
<div class="col-xs-12 col-sm-9 col-centered">
<div class="panel panel-default panel-default center-block">
<div class="panel-body">
<h1 class="color-primary"><?php print render($content['field_volunteers_title']); ?></h1>
<div class="row row-centered">
<div class="col-xs-12 col-sm-6">
<table class="table table-condensed">
<tr><th><?php print render($content['field_last_name_text']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterVolunteer()"></td></tr>
<tr><th><?php print render($content['field_first_name_text']); ?></th><td><input type="text"  style="width:100%;" onkeypress="modifyFilterVolunteer()"></td></tr>
<tr><th><?php print render($content['field_city_text']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterVolunteer()"></td></tr>
<tr><th><?php print render($content['field_zip_text']); ?></th><td>
<select style="width:100%;" onchange="modifyFilterVolunteer()">
  <option value="ALL"><?php print render($content['field_all']); ?></option>
  <option value="1"><?php print render($content['field_brussels_capital_region']); ?></option>
  <option value="2"><?php print render($content['field_walloon_brabant']); ?></option>
  <option value="3"><?php print render($content['field_flemish_brabant']); ?></option>
  <option value="4"><?php print render($content['field_antwerp']); ?></option>
  <option value="5"><?php print render($content['field_limburg']); ?></option>
  <option value="6"><?php print render($content['field_liege']); ?></option>
  <option value="7"><?php print render($content['field_namur']); ?></option>
  <option value="8"><?php print render($content['field_hainaut']); ?></option>
  <option value="9"><?php print render($content['field_luxembourg']); ?></option>
  <option value="10"><?php print render($content['field_west_flanders']); ?></option>
  <option value="11"><?php print render($content['field_east_flanders']); ?></option>
</select>
</td></tr>
<tr><th><?php print render($content['field_disponibility_text']); ?></th><td><select style="width:100%;" onchange="modifyFilterVolunteer()"><option value="ALL" selected="selected" onchange="modifyFilterVolunteer()"><?php print render($content['field_all']); ?></option><option value="0"><?php print render($content['field_no']); ?></option><option value="1"><?php print render($content['field_yes']); ?></option></select></td></tr>
<tr><th><?php print render($content['field_language_text']); ?></th><td><select style="width:100%;" onchange="modifyFilterVolunteer()"><option value="ALL" selected="selected" onchange="modifyFilterVolunteer()"><?php print render($content['field_all']); ?></option><option value="EN">EN</option><option value="FR">FR</option><option value="NL">NL</option></select></td></tr>
<tr><th><?php print render($content['field_activity_sector_text']); ?></th><td><select style="width:100%;" onchange="modifyFilterVolunteer()">
  <option value="ALL" selected="selected"><?php print render($content['field_all']); ?></option>
  <option value="1"><?php print render($content['field_horeca']); ?></option>
  <option value="2"><?php print render($content['field_insurance']); ?></option>
  <option value="3"><?php print render($content['field_import_export']); ?></option>
  <option value="4"><?php print render($content['field_transport']); ?></option>
  <option value="5"><?php print render($content['field_cosmetic']); ?></option>
  <option value="6"><?php print render($content['field_art']); ?></option>
  <option value="7"><?php print render($content['field_personal_consultancy']); ?></option>
  <option value="8"><?php print render($content['field_small_shop']); ?></option>
  <option value="9"><?php print render($content['field_building']); ?></option>
  <option value="10"><?php print render($content['field_hairdressing']); ?></option>
  <option value="11"><?php print render($content['field_textile']); ?></option>
  <option value="12"><?php print render($content['field_other']); ?></option>
</select><td></tr>
<tr><th><?php print render($content['field_competence_text']); ?></th><td><select style="width:100%;" onchange="modifyFilterVolunteer()">
  <option value="ALL" selected="selected"><?php print render($content['field_all']); ?></option>
  <option value="1"><?php print render($content['field_business_model_development']); ?></option>
  <option value="2"><?php print render($content['field_financial_plan_expertise']); ?></option>
  <option value="3"><?php print render($content['field_startup_administration']); ?></option>
  <option value="4"><?php print render($content['field_daily_administration_busin']); ?></option>
  <option value="5"><?php print render($content['field_financial_followup']); ?></option>
  <option value="6"><?php print render($content['field_accountancy']); ?></option>
  <option value="7"><?php print render($content['field_marketing']); ?></option>
  <option value="8"><?php print render($content['field_communication']); ?></option>
  <option value="9"><?php print render($content['field_legal_expertise']); ?></option>
  <option value="10"><?php print render($content['field_human_resource']); ?></option>
  <option value="11"><?php print render($content['field_other']); ?></option>
</select><td></tr>
<tr><th><?php print render($content['field_mission_text']); ?></th><td><select style="width:100%;" onchange="modifyFilterVolunteer()">
  <option value="ALL" selected="selected"><?php print render($content['field_all']); ?></option>
  <option value="1"><?php print render($content['field_mission_project_planning']); ?></option>
  <option value="2"><?php print render($content['field_mission_startup']); ?></option>
  <option value="3"><?php print render($content['field_mission_expert']); ?></option>
  <option value="4"><?php print render($content['field_mission_coach']); ?></option>
  <option value="5"><?php print render($content['field_mission_animator']); ?></option>
  <option value="6"><?php print render($content['field_mission_former']); ?></option>
  <option value="7"><?php print render($content['field_mission_president']); ?></option>
  <option value="8"><?php print render($content['field_mission_recouvrement']); ?></option>
  <option value="9"><?php print render($content['field_mission_internal_com']); ?></option>
  <option value="10"><?php print render($content['field_mission_ambassador']); ?></option>
  <option value="11"><?php print render($content['field_mission_it_developer']); ?></option>
  <option value="12"><?php print render($content['field_mission_search_funding']); ?></option>
  <option value="13"><?php print render($content['field_mission_reception']); ?></option>
  <option value="14"><?php print render($content['field_mission_coach_of_coach']); ?></option>
  <option value="15"><?php print render($content['field_mission_administrator_awlp']); ?></option>
</select><td></tr>
</table>
</div><!-- /.col -->
</div><!-- /.row -->

<div id="volunteer-map-section" style="padding-bottom:10px;display:none;">
<div class="row row-centered">
<div class="col-xs-12">
<div class="panel panel-default panel-default center-block">
<div class="panel-body" style="padding:0;">
<div id="volunteer-map" style="width:100%;height:300px;"></div>
</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->
</div>

<div class="row row-centered">
<div class="col-xs-12">
<div id="ajax-target-volunteer"></div>
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body -->
</div><!-- /.panel -->
  </div><!-- /.ct_c_coaching_management -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLET5LRDvQP5wCsd8uBKuOOC_bSnEQn3I"></script>
<script type="text/javascript">
function changeTabHeaderLabel() {
  (function ($) {
    $('div.select_volunteer_label').replaceWith( '<?php print render($content['field_select_header']); ?>' );
    $('div.last_name_label').replaceWith( '<?php print render($content['field_last_name_text']); ?>' );
    $('div.first_name_label').replaceWith( '<?php print render($content['field_first_name_text']); ?>' );
    $('div.city_label').replaceWith( '<?php print render($content['field_city_text']); ?>' );
    $('div.zip_label').replaceWith( '<?php print render($content['field_zip_text']); ?>' );
    $('div.disponibility_label').replaceWith( '<?php print render($content['field_disponibility_text']); ?>' );
    $('div.language_label').replaceWith( '<?php print render($content['field_language_text']); ?>' );
    $('div.activity_sector_label').replaceWith( '<?php print render($content['field_activity_sector_text']); ?>' );
    $('div.competence_label').replaceWith( '<?php print render($content['field_competence_text']); ?>' );
    $('div.email_label').replaceWith( '<?php print render($content['field_email_text']); ?>' );
    $('div.phone_label').replaceWith( '<?php print render($content['field_phone_text']); ?>' );
    $('div.mission_label').replaceWith( '<?php print render($content['field_mission_text']); ?>' );
    $('div.photo_label').replaceWith( '<?php print render($content['field_photo_text']); ?>' );
  })(jQuery);
}
function changeActivitySectorLabel() {
  (function ($) {
    $('div.activity_sector_label_1').replaceWith( '<?php print render($content['field_horeca']); ?>' );
    $('div.activity_sector_label_2').replaceWith( '<?php print render($content['field_insurance']); ?>' );
    $('div.activity_sector_label_3').replaceWith( '<?php print render($content['field_import_export']); ?>' );
    $('div.activity_sector_label_4').replaceWith( '<?php print render($content['field_transport']); ?>' );
    $('div.activity_sector_label_5').replaceWith( '<?php print render($content['field_cosmetic']); ?>' );
    $('div.activity_sector_label_6').replaceWith( '<?php print render($content['field_art']); ?>' );
    $('div.activity_sector_label_7').replaceWith( '<?php print render($content['field_personal_consultancy']); ?>' );
    $('div.activity_sector_label_8').replaceWith( '<?php print render($content['field_small_shop']); ?>' );
    $('div.activity_sector_label_9').replaceWith( '<?php print render($content['field_building']); ?>' );
    $('div.activity_sector_label_10').replaceWith( '<?php print render($content['field_hairdressing']); ?>' );
    $('div.activity_sector_label_11').replaceWith( '<?php print render($content['field_textile']); ?>' );
    $('div.activity_sector_label_12').replaceWith( '<?php print render($content['field_other']); ?>' );
  })(jQuery);
}
function changeCompetenceLabel() {
  (function ($) {
    $('div.competence_label_1').replaceWith( '<?php print render($content['field_business_model_development']); ?>' );
    $('div.competence_label_2').replaceWith( '<?php print render($content['field_financial_plan_expertise']); ?>' );
    $('div.competence_label_3').replaceWith( '<?php print render($content['field_startup_administration']); ?>' );
    $('div.competence_label_4').replaceWith( '<?php print render($content['field_daily_administration_busin']); ?>' );
    $('div.competence_label_5').replaceWith( '<?php print render($content['field_financial_followup']); ?>' );
    $('div.competence_label_6').replaceWith( '<?php print render($content['field_accountancy']); ?>' );
    $('div.competence_label_7').replaceWith( '<?php print render($content['field_marketing']); ?>' );
    $('div.competence_label_8').replaceWith( '<?php print render($content['field_communication']); ?>' );
    $('div.competence_label_9').replaceWith( '<?php print render($content['field_legal_expertise']); ?>' );
    $('div.competence_label_10').replaceWith( '<?php print render($content['field_human_resource']); ?>' );
    $('div.competence_label_11').replaceWith( '<?php print render($content['field_other']); ?>' );
  })(jQuery);
}
function changeMissionLabel() {
  (function ($) {
    $('div.mission_label_1').replaceWith( '<?php print render($content['field_mission_project_planning']); ?>' );
    $('div.mission_label_2').replaceWith( '<?php print render($content['field_mission_startup']); ?>' );
    $('div.mission_label_3').replaceWith( '<?php print render($content['field_mission_expert']); ?>' );
    $('div.mission_label_4').replaceWith( '<?php print render($content['field_mission_coach']); ?>' );
    $('div.mission_label_5').replaceWith( '<?php print render($content['field_mission_animator']); ?>' );
    $('div.mission_label_6').replaceWith( '<?php print render($content['field_mission_former']); ?>' );
    $('div.mission_label_7').replaceWith( '<?php print render($content['field_mission_president']); ?>' );
    $('div.mission_label_8').replaceWith( '<?php print render($content['field_mission_recouvrement']); ?>' );
    $('div.mission_label_9').replaceWith( '<?php print render($content['field_mission_internal_com']); ?>' );
    $('div.mission_label_10').replaceWith( '<?php print render($content['field_mission_ambassador']); ?>' );
    $('div.mission_label_11').replaceWith( '<?php print render($content['field_mission_it_developer']); ?>' );
    $('div.mission_label_12').replaceWith( '<?php print render($content['field_mission_search_funding']); ?>' );
    $('div.mission_label_13').replaceWith( '<?php print render($content['field_mission_reception']); ?>' );
    $('div.mission_label_14').replaceWith( '<?php print render($content['field_mission_coach_of_coach']); ?>' );
    $('div.mission_label_15').replaceWith( '<?php print render($content['field_mission_administrator_awlp']); ?>' );
  })(jQuery);
}
function hideSelectVolunteer() {
  (function ($) {
    $('th.select_volunteer').hide();
    $('td.select_volunteer').hide();
  })(jQuery);
}
function modifyFilterVolunteer() {
  (function ($) {
    $("#ajax-target-volunteer").load('ajax/volunteer_list_filtered/' + '1' + '/' + '2' + '/' + '3' + '/' + '4' + '/' + '5' + '/' + '6' + '/' + '7', function() {
      changeTabHeaderLabel();
      changeActivitySectorLabel();
      changeCompetenceLabel();
      changeMissionLabel();
      hideSelectVolunteer();
      generate_map();
    });
  })(jQuery);
}
function block_volunteer_select() {
  (function ($) {
  })(jQuery);
}
function generate_map() {
  (function ($) {
    $("#volunteer-map-section").show();
    $("#volunteer-map").googleMap({
      zoom: 10, // Initial zoom level (optional)
      coords: [50.8503463, 4.3517211], // Map center (optional)
      type: "ROADMAP" // Map type (optional)
    });
    $( ".volunteer-address" ).each(function( index ) {
      $("#volunteer-map").addMarker({
        address: $(this).html(), // Postale Address
        title: $(this).attr('name'), // Title
      });
    });
<?php
foreach ($content['field_ms_agency_address'] as $key => $value):
if (is_numeric($key)):
  print '$("#volunteer-map").addMarker({';
  print 'address: \'' . render($content['field_ms_agency_address'][$key]) . '\','; // Postale Address';
  print 'icon: \'' . file_create_url($node->field_ms_pinpoint['und'][0]['uri']) . '\''; // Icon URL
  print '});';
endif;
endforeach;
?>
  })(jQuery);
}
</script>

<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_gender_text'] );
  hide ( $content ['field_birthday_text'] );
  hide ( $content ['field_address_text'] );
  hide ( $content ['field_phone_text'] );
  hide ( $content ['field_email_text'] );
  hide ( $content ['field_language_text'] );
  hide ( $content ['field_is_english_text'] );
  hide ( $content ['field_is_degree_in_belgium_text'] );
  hide ( $content ['field_is_mgt_exam_passed_text'] );
  hide ( $content ['field_profesionnal_status_text'] );
  hide ( $content ['field_project_description_text'] );
  hide ( $content ['field_funding_reason_text'] );
  hide ( $content ['field_partner_comment_text'] );
  hide ( $content ['field_project_starting_date_text'] );
  hide ( $content ['field_tva_number_text'] );
  hide ( $content ['field_project_status_text'] );
  hide ( $content ['field_actvity_sector_text'] );
  hide ( $content ['field_amount_text'] );
  hide ( $content ['field_duration_text'] );
  hide ( $content ['field_national_bank_situation_te'] );
  hide ( $content ['field_download_text'] );
  hide ( $content ['field_doc_mandate_text'] );
  hide ( $content ['field_doc_ibi_text'] );
  hide ( $content ['field_doc_gad_text'] );
  hide ( $content ['field_doc_action_plan'] );
  hide ( $content ['field_doc_training_history_text '] );
  hide ( $content ['field_no_file_text'] );
  hide ( $content ['field_coach_text']);
  hide ( $content ['field_coaching_ending_date_text']);
  hide ( $content ['field_last_feedback_text'] );
  ?>
  <?php $client = microstart_saleforce_getClient($_GET["client_id"]); ?>
  <?php $advisor = microstart_saleforce_getAdvisor($client['advisor_id']); ?>
  <?php $volunteer = microstart_saleforce_getVolunteer($client['coach_id']); ?>
  <?php $training_list = microstart_saleforce_client_training_list_filtered(null, null, null, $client['client_id']); ?>
  <?php $feedback_list = microstart_saleforce_feedback_list_filtered(null, null, null, null, $client['client_id']); ?>
  <div class="ct_c_client_details">
    <div class="panel panel-default panel-microstart-back center-block"
      style="width: 98vw;">
      <div class="panel-body">
        <div class="row row-centered" style="padding-bottom:10px;">
          <div class="col-xs-12 col-sm-5 col-centered">
            <div class="panel panel-default">
              <div class="panel-body">
                <h1 class="color-primary" style="text-align:center;"><?php print $client['first_name'] . ' ' . $client['last_name']; ?></h1>
                <table class="table table-condensed table-striped">
                  <tr>
                    <th><?php print render($content ['field_gender_text']); ?></th>
                    <td><?php print $client['gender']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_birthday_text']); ?></th>
                    <td><?php print $client['borndate']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_address_text']); ?></th>
                    <td><?php print $client['street'] . ' ' . $client['zip'] .  ' ' . $client['city']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_phone_text']); ?></th>
                    <td><?php print $client['phone']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_email_text']); ?></th>
                    <td><?php print $client['email']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_language_text']); ?></th>
                    <td><?php print $client['language']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_is_english_text']); ?></th>
                    <td><?php print $client['is_contacted_in_english']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_is_degree_in_belgium_text']); ?></th>
                    <td><?php print $client['is_degree_in_belgium']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_is_mgt_exam_passed_text']); ?></th>
                    <td><?php print $client['is_management_exam_passed']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_profesionnal_status_text']); ?></th>
                    <td><?php print $client['profesionnal_status']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_advisor_name']); ?></th>
                    <td><?php print $advisor['first_name'] . ' ' . $advisor['last_name']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_coach_text']); ?></th>
                    <td><?php print $volunteer['first_name'] . ' ' . $volunteer['last_name']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_coaching_ending_date_text']); ?></th>
                    <td><?php print $client['coaching_ending_date'] ?></td>
                  </tr>
                </table>
                <table class="table table-condensed">
                  <tr>
                    <th><?php print render($content ['field_doc_training_history_text']); ?></th>
                  </tr>
                  <tr>
                    <td>
<?php
if (count($training_list) == 0) {
  print '<div>/</div>';
} else {
  foreach ($training_list as $training){
    print '<div>'  . $training['training_title'] . '</div>';
  }
}
 ?>
                    </td>
                  </tr>
                </table>
                <table class="table table-condensed">
                  <tr>
                    <th><?php print render($content ['field_last_feedback_text']); ?></th>
                  </tr>
                  <tr>
                    <td>
<?php
$counter = -1;
$counter_date = new DateTime('01/01/1900');
for ($i = 0; $i < count($feedback_list); $i++) {
  $new_date = new DateTime($feedback_list[$i]['date']);
  if ($counter_date->format('m/d/Y') < $new_date->format('m/d/Y')) {
    $counter_date = new DateTime($feedback_list[$i]['date']);
    $counter = $i;
  }
}
if ($counter != -1) {
  print '<div>'  . $feedback_list[$counter]['feedback'] . '</div>';
} else {
  print '<div>/</div>';
}
 ?>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-->
        </div>
        <!-- /.row -->
        <div class="row row-centered">
          <div class="col-xs-12 col-sm-5 col-centered">
            <div class="panel panel-default">
              <div class="panel-body">
                <h1 class="color-primary" style="text-align:center;"><?php print $client['project_name']; ?></h1>
                <table class="table table-condensed">
                  <tr>
                    <th><?php print render($content ['field_project_description_text']); ?></th>
                  </tr>
                  <tr>
                    <td><?php print $client['project_description']; ?></td>
                  </tr>
                </table>
                <table class="table table-condensed">
                  <tr>
                    <th><?php print render($content ['field_partner_comment_text']); ?></th>
                  </tr>
                  <tr>
                    <td><?php print $client['partner_comment']; ?></td>
                  </tr>
                </table>
                <table class="table table-condensed table-striped">
                  <tr>
                    <th><?php print render($content ['field_project_starting_date_text']); ?></th>
                    <td><?php print $client['project_starting_date']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_tva_number_text']); ?></th>
                    <td><?php print $client['tva_number']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_project_status_text']); ?></th>
                    <td><?php print $client['project_status']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_actvity_sector_text']); ?></th>
                    <td><?php print $client['activity_sector']; ?></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_doc_mandate_text']); ?></th>
                    <td><button type="submit" class="btn btn-primary button-sharp background-primary-color btn-xs"><?php print render($content ['field_download_text']); ?></button></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_doc_ibi_text']); ?></th>
                    <td><button type="submit" class="btn btn-primary button-sharp background-primary-color btn-xs"><?php print render($content ['field_download_text']); ?></button></td>
                  </tr>
                  <tr>
                    <th><?php print render($content ['field_doc_gad_text']); ?></th>
                    <td><button type="submit" class="btn btn-primary button-sharp background-primary-color btn-xs"><?php print render($content ['field_download_text']); ?></button></td>
                  </tr>
                </table>
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.ct_c_client_details -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_clients_title']);
    hide($content['field_last_name_text']);
    hide($content['field_first_name_text']);
    hide($content['field_select_header']);
    hide($content['field_last_name_header']);
    hide($content['field_first_name_header']);
    hide($content['field_email_header']);
    hide($content['field_phone_header']);
    hide($content['field_coach_header']);
    hide($content['field_coaching_ending_date']);
    hide($content['field_only_coached']);
  ?>
  <div class="ct_c_client_list">

<div class="panel panel-default panel-microstart-back center-block">
<div class="panel-body">

<div id="block_client" class="row row-centered" style="padding-bottom:10px;">
<div class="col-xs-12 col-sm-11 col-centered">
<div class="panel panel-default panel-default center-block">
<div class="panel-body">
<h1 class="color-primary"><?php print render($content['field_clients_title']); ?></h1>
<div class="row row-centered">
<div class="col-xs-12 col-sm-6" style="padding-bottom=10px;">
<table class="table table-condensed" style="text-align:left;">
<tr><th><?php print render($content['field_last_name_text']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterClient()"></td></tr>
<tr><th><?php print render($content['field_first_name_text']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterClient()"></td></tr>
<tr><th><?php print render($content['field_coach_last_name']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterClient()"></td></tr>
<tr><th><?php print render($content['field_coach_first_name']); ?></th><td><input type="text" style="width:100%;" onkeypress="modifyFilterClient()"></td></tr>
<tr><th><?php print render($content['field_only_coached']); ?></th><td><input type="checkbox" name="only_coached" value="only_coached" onChange="modifyFilterClient()"/></td></tr>
</table>
</div><!-- /.col -->
</div><!-- /.row -->
<div class="row row-centered">
<div class="col-xs-12 col-sm-12">
<div id="ajax-target-client"></div>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body -->
</div><!-- /.panel -->
  </div><!-- /.ct_c_client_list -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript">
function changeTabHeaderLabel() {
  (function ($) {
    $('div.select_client_label').replaceWith( '<?php print render($content['field_select_header']); ?>' );
    $('div.last_name_label').replaceWith( '<?php print render($content['field_last_name_header']); ?>' );
    $('div.first_name_label').replaceWith( '<?php print render($content['field_first_name_header']); ?>' );
    $('div.email_label').replaceWith( '<?php print render($content['field_email_header']); ?>' );
    $('div.phone_label').replaceWith( '<?php print render($content['field_phone_header']); ?>' );
    $('div.coach_label').replaceWith( '<?php print render($content['field_coach_header']); ?>' );
    $('div.coaching_ending_date_label').replaceWith( '<?php print render($content['field_coaching_ending_date']); ?>' );
  })(jQuery);
}
function modifyFilterClient() {
  (function ($) {
    $("#ajax-target-client").load('ajax/client_list_filtered_complex/' + '1' + '/' + '2' + '/' + '3' + '/' + '4' + '/' + '5', function() {
      changeTabHeaderLabel();
    });
  })(jQuery);
}
function block_client_select() {
  (function ($) {
    window.location.href = 'client-details?client_id=' + $("input:radio[name='client_selection']:checked").val();
  })(jQuery);
}
</script>
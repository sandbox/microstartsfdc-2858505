<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_credits_title'] );
  hide ( $content ['field_contact_advisor_image'] );
  hide ( $content ['field_contact_advisor_text'] );
  hide ( $content ['field_credit_amount_text'] );
  hide ( $content ['field_credit_amount_delay_text'] );
  hide ( $content ['field_payment_text'] );
  hide ( $content ['field_details_text'] );
  hide ( $content ['field_no_credit_text'] );
  ?>
  <?php $client = microstart_saleforce_getClient(null); ?>
  <?php $advisor = microstart_saleforce_getAdvisor($client['advisor_id']); ?>
  <?php $credit_list = microstart_saleforce_credit_management_list(); ?>
  <div class="ct_c_credit_management_list">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

<div class="row row-centered">

<div class="col-xs-11 col-centered microstart-font-size">
<div class="color-primary"><h1><?php print render($content['field_credits_title']) ?></h1></div>
</div><!-- /.col -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-centered-centered col-top" style="padding-bottom:10px;">
<?php $content['field_contact_advisor_image'][0]['#item']['attributes']['class'][] = "center-block img-circle"; ?>
<?php $content['field_contact_advisor_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;"; ?>
<?php print '<div style="padding-bottom:10px;">' . render($content['field_contact_advisor_image']) . '</div>'; ?>
<?php print '<div class="microstart-button" style="padding-bottom:10px;"><a href="mailto:' . $advisor['email'] . '">'; ?>
<?php print '<div class="panel panel-default button-sharp microstart-button-font-size background-color-primary"><div class="panel-body">' . render($content['field_contact_advisor_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->

<?php if (count($credit_list) <= 0): ?>
<?php print '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-centered-centered col-top microstart-font-size" style="font-weight:bold;"><div class="panel panel-default"><div class="panel-body">';  ?>
<?php print render($content ['field_no_credit_text']); ?>
<?php print '</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col -->';  ?>
<?php endif; ?>

<?php if (count($credit_list) > 0): ?>
<?php print '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-centered col-top">';  ?>
<?php print '<div class="row row-centered">';  ?>

<?php foreach ( $credit_list as $credit): ?>
<?php print '<div class="col-xs-12 col-centered" style="padding-bottom:10px;">';  ?>

<?php print '<div class="panel panel-default"><div class="panel-body">';  ?>
<?php print '<div class="row row-centered">';  ?>
<?php print '<div class="col-xs-12 col-sm-3 col-centered">';  ?>
<?php print '<h1>' . $credit['cred_ref'] . '</h1>'; ?>
<?php print '</div><!-- /.col -->';  ?>
<?php print '<div class="col-xs-12 col-sm-3 col-centered">';  ?>
<?php print render($content ['field_credit_amount_text']); ?>
<?php print '<div>' . $credit['cred_amount'] .' €</div>'; ?>
<?php print '</div><!-- /.col -->';  ?>
<?php print '<div class="col-xs-12 col-sm-5 col-centered">';  ?>
<?php print render($content ['field_credit_amount_delay_text']); ?>
<?php print '<div style="color:red;">' . ($credit['cred_term_amount'] * $credit['cred_qty_term_delayed'])  .' €</div>'; ?>
<?php print '</div><!-- /.col -->';  ?>
<?php print '</div><!-- /.row -->';  ?>

<?php print '<div style="float:right;">'; ?>
<?php print '<span style="padding-right:10px">'; ?>
<?php print '<button type="button"class="btn btn-warning background-color-secondary button-sharp" onclick="window.location.href=\'/credit-management-payement?cred_ref=' . $credit['cred_ref'] . '\'" style="font-size:150%;">'; ?>
<?php print render($content['field_payment_text']); ?>
<?php print '</button></span>'; ?>
<?php print '<button type="button" class="btn btn-primary background-color-primary button-sharp" onclick="window.location.href=\'/credit-management?cred_ref=' . $credit['cred_ref'] . '\'" style="font-size:150%;">'; ?>
<?php print render($content['field_details_text']); ?>
<?php print '</button>'; ?>
<?php print '</div>'; ?>

<?php print '</div><!-- /.panel-body --></div><!-- /.panel -->';  ?>

<?php print '</div><!-- /.col -->';  ?>
<?php endforeach; ?>

<?php print '</div><!-- /.row -->';  ?>
<?php print '</div><!-- /.col -->';  ?>
<?php endif; ?>

<?php
$nid = node_view(node_load(68));
$qty=0;
foreach ($nid['field_client_image'] as $key => $value) {
  if (is_numeric($key)) {
    $qty++;
  }
}
$i = 0;
if ($i < $qty) {
  $key = rand  (0, $qty-1);
  $nid['field_client_image'][$key]['#item']['attributes']['class'][] = "center-block img-circle";
  $nid['field_client_image'][$key]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
}
?>
<div class="col-xs-12 col-sm-3 col-centered col-top">
<?php print '<div style="padding-bottom:10px;">' . render($nid['field_client_image'][$key]) . '</div>'; ?>
</div><!-- /.col -->


</div><!-- /.row -->

  </div><!-- /.panel-body -->
  </div><!-- /.panel -->
  </div><!-- /.ct_c_credit_management_list -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>

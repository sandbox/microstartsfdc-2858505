<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_prospects_title'] );
  hide ( $content ['field_no_prospect_text'] );
  hide ( $content ['field_partner_all_text'] );
  hide ( $content ['field_last_name_header'] );
  hide ( $content ['field_first_name_header'] );
  hide ( $content ['field_email_header'] );
  hide ( $content ['field_phone_header'] );
  hide ( $content ['field_partner_name_text'] );
  hide ( $content ['field_request_status_text'] );
  hide ( $content ['field_status_waiting'] );
  hide ( $content ['field_status_cancelled'] );
  hide ( $content ['field_status_accepted'] );
  hide ( $content ['field_date_header'] );
  hide ( $content ['field_advisor_comment'] );
  ?>
  <?php $prospect_list = microstart_saleforce_prospect_list(); ?>
  <div class="ct_c_credit_management_list">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

<div class="row row-centered">

<div class="col-xs-11 col-centered microstart-font-size">
<div class="color-primary"><h1><?php print render($content['field_prospects_title']) ?></h1></div>
</div><!-- /.col -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-top" style="padding-bottom:10px;font-size:200%;">
<?php $content['field_contact_microstart_image'][0]['#item']['attributes']['class'][] = "center-block img-circle"; ?>
<?php $content['field_contact_microstart_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;"; ?>
<?php print '<div style="padding-bottom:10px;">' . render($content['field_contact_microstart_image']) . '</div>'; ?>
<?php print '<div class="microstart-button" style="padding-bottom:10px;"><a href="http://microstart.be/fr/contact">'; ?>
<?php print '<div class="panel panel-default button-sharp background-color-primary"><div class="panel-body">' . render($content['field_contact_microstart_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->

<!-- BLOCK CENTER -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
<!-- BLOCK CENTER -->


<div class="row row-centered-centered" style="padding-bottom:10px;">
<div class="col-xs-12 col-centered-centered">
<div style="display:inline-block;"><?php print render($content['field_partner_name_text']); ?></div>
<div style="display:inline-block;">
<select id="filter-partner" class="form-control input-sm" onchange="modifyFilter()">
<option value="0"><?php print render($content['field_partner_all_text']); ?></option>
<?php
$partner_user_list = array();
for ($i = 0; $i < count($prospect_list); $i++) {
  $partner_name = $prospect_list[$i]['partner_first_name'] . ' ' . $prospect_list[$i]['partner_last_name'];
  if (! in_array($partner_name, $partner_user_list)) {
    $partner_user_list[$partner_name] = $i;
    print '<option value="' . ($i + 1) . '">' . $partner_name . '</option>';
  }
}
?>
</select>
</div>
</div><!-- /.col -->
</div><!-- /.row -->

<?php if (count($prospect_list) <= 0): ?>
<?php print '<div class="row row-centered"><div class="col-xs-12 col-centered-centered"><div class="panel panel-default"><div class="panel-body">';  ?>
<?php print render($content ['field_no_prospect_text']); ?>
<?php print '</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php endif; ?>

<?php if (count($prospect_list) > 0): ?>
<?php foreach ( $prospect_list as $prospect): ?>
<?php print '<div class="row row-centered"  style="padding-bottom:10px;"><div class="col-xs-12 col-centered"><div class="panel panel-default"><div class="panel-body"><div class="table-responsive"><table class="table table-condensed" style="margin:0;">'; ?>
<?php print '<tr class="prospect partner-' . ($partner_user_list[$prospect['partner_first_name'] . ' ' . $prospect['partner_last_name']] + 1) . '">'; ?>
<?php print '<th>' . render($content ['field_last_name_header']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_first_name_header']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_email_header']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_phone_header']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_partner_name_text']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_date_header']) . '</th>'; ?>
<?php print '<th>' . render($content ['field_request_status_text']) . '</th>'; ?>
<?php print '</tr>'; ?>
<?php print '<tr class="prospect partner-' . ($partner_user_list[$prospect['partner_first_name'] . ' ' . $prospect['partner_last_name']] + 1) . '">'; ?>
<?php print '<td>' . $prospect['last_name'] . '</td>'; ?>
<?php print '<td>' . $prospect['first_name'] . '</td>'; ?>
<?php print '<td>' . $prospect['email'] . '</td>'; ?>
<?php print '<td>' . $prospect['phone'] . '</td>'; ?>
<?php print '<td>' . $prospect['partner_first_name'] . ' ' . $prospect['partner_last_name'] . '</td>'; ?>
<?php print '<td>' . $prospect['creation_date'] . '</td>'; ?>
<?php
switch ($prospect['request_status']) {
    case 1:
        $request_status = render($content ['field_status_waiting']);
        break;
    case 2:
        $request_status = render($content ['field_status_cancelled']);
        break;
    case 3:
        $request_status = render($content ['field_status_accepted']);
        break;
    default:
        $request_status = t('Unknow');
  }
?>
<?php print '<td>' . $request_status . '</td>'; ?>
<?php print '</tr>'; ?>
<?php if ((isset($prospect['advisor_comment'])) && ($prospect['advisor_comment'] != '')): ?>
<?php print '<tr>'; ?>
<?php print '<th colspan="7">' . render($content ['field_advisor_comment']) . '</th>'; ?>
<?php print '</tr>'; ?>
<?php print '<tr>'; ?>
<?php print '<td colspan="7">' . $prospect['advisor_comment'] . '</td>'; ?>
<?php print '</tr>'; ?>
<?php endif; ?>
<?php print '</table></div><!-- /.table-responsive --></div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php endforeach; ?>
<?php endif; ?>

<!-- BLOCK CENTER -->
</div><!-- /.col -->
<!-- BLOCK CENTER -->

<?php
$nid = node_view(node_load(68));
$qty=0;
foreach ($nid['field_client_image'] as $key => $value) {
  if (is_numeric($key)) {
    $qty++;
  }
}
$i = 0;
if ($i < $qty) {
  $key = rand  (0, $qty-1);
  $nid['field_client_image'][$key]['#item']['attributes']['class'][] = "center-block img-circle";
  $nid['field_client_image'][$key]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
}
?>
<div class="col-xs-12 col-sm-3 col-top col-centered-centered">
<?php print '<div style="padding-bottom:10px;">' . render($nid['field_client_image'][$key]) . '</div>'; ?>
</div><!-- /.col -->


</div><!-- /.row -->

  </div><!-- /.panel-body -->
  </div><!-- /.panel -->
  </div><!-- /.ct_c_credit_management_list -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript">
function modifyFilter() {
  (function ($) {
    if ($('#filter-partner').val() == 0) {
      $(".prospect").show();
    } else {
      $(".prospect").show();
      $(".prospect").not('.partner-' + $('#filter-partner').val()).hide();
    }
  })(jQuery);
}
</script>

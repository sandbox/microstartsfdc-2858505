<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>"
  class="<?php print $classes; ?> clearfix" <?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
  // Hide comments, tags, and links now so that we can render them later.
  hide ( $content ['comments'] );
  hide ( $content ['links'] );
  hide ( $content ['field_tags'] );
  hide ( $content ['field_credit_title'] );
  hide ( $content ['field_contact_advisor_image'] );
  hide ( $content ['field_contact_advisor_text'] );
  hide ( $content ['field_cred_first_term_text'] );
  hide ( $content ['field_cred_last_term_text'] );
  hide ( $content ['field_legend_payed_text'] );
  hide ( $content ['field_legend_delay_text'] );
  hide ( $content ['field_cred_qty_term_remaining'] );
  hide ( $content ['field_cred_capital_reimbursed'] );
  hide ( $content ['field_cred_term_amount_text'] );
  hide ( $content ['field_cred_disbursement_date'] );
  hide ( $content ['field_cred_amount_text'] );
  hide ( $content ['field_cred_status_text'] );
  hide ( $content ['field_cred_capital_delay_text'] );
  hide ( $content ['field_cred_next_term_date_text'] );
  hide ( $content ['field_cred_remaining_cost_text'] );
  hide ( $content ['field_nova_report_text'] );
  hide ( $content ['field_image_pay'] );
  hide ( $content ['field_link_pay'] );
  hide ( $content ['field_warning_payement_refused'] );
  ?>
  <?php $client = microstart_saleforce_getClient(null); ?>
  <?php $advisor = microstart_saleforce_getAdvisor($client['advisor_id']); ?>
  <?php $credit = microstart_saleforce_credit_management($_GET["cred_ref"]); ?>
  <?php $credit_general_info = $credit['general_info']; ?>
  <?php $credit_count = $credit['count']; ?>
  <?php $credit_timetable_initial = $credit['timetable_initial']; ?>
  <div class="ct_c_credit_management">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

<div class="row row-centered">

<div class="col-xs-11 col-centered microstart-font-size">
<div class="color-primary"><h1><div style="display:inline-block"><?php print render($content['field_credit_title']) ?></div> <div style="display:inline-block"><?php print $credit_general_info['cred_ref']; ?></div></h1></div>
</div><!-- /.col -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-centered-centered col-top" style="padding-bottom:10px;">
<?php $content['field_contact_advisor_image'][0]['#item']['attributes']['class'][] = "center-block img-circle"; ?>
<?php $content['field_contact_advisor_imagee'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;"; ?>
<?php print '<div style="padding-bottom:10px;">' . render($content['field_contact_advisor_image']) . '</div>'; ?>
<?php print '<div class="microstart-button" style="padding-bottom:10px;"><a href="mailto:' . $advisor['email'] . '">'; ?>
<?php print '<div class="panel panel-default button-sharp microstart-button-font-size" style="background-color:#3B5998;"><div class="panel-body">' . render($content['field_contact_advisor_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-centered-centered col-top">

<div class="row row-centered" style="padding-bottom:10px;"><div class="col-xs-12 col-centered"><div class="panel panel-default"><div class="panel-body">

<div class="row row-centered">
<div class="col-xs-6" style="text-align:left;">
<div><?php print render($content['field_cred_first_term_text']); ?></div>
<div><?php print $credit_timetable_initial[0]['transfert_date']; ?></div>
</div><!-- /.col -->
<div class="col-xs-6" style="text-align:right;">
<div><?php print render($content['field_cred_last_term_text']); ?></div>
<div><?php print $credit_timetable_initial[(count($credit_timetable_initial) -1)]['transfert_date']; ?></div>
</div><!-- /.col -->
</div><!-- /.row -->
<div class="row row-centered" style="padding-bottom:10px;">
<div class="col-xs-12 col-sm-12 col-centered">
<div class="progress" style="margin-bottom:0;">
<?php $percent_payed = number_format((($credit_general_info['cred_capital_reimbursed']/$credit_general_info['cred_initial_cost']) * 100), 2); ?>
<?php $percent_delay = number_format(((($credit_general_info['cred_term_amount'] * $credit_general_info['cred_qty_term_delayed'])/$credit_general_info['cred_initial_cost']) * 100), 2); ?>
<div class="progress-bar background-color-primary" role="progressbar" style="width:<?php print $percent_payed; ?>%"><?php print $percent_payed; ?>%</div>
<div class="progress-bar background-color-secondary" role="progressbar" style="width:<?php print $percent_delay; ?>%"><?php print $percent_delay; ?>%</div>
</div>
<?php print '<div class="color-primary" style="display:inline-block">' . render($content['field_legend_payed']) . '</div> / <div class="color-secondary" style="display:inline-block">' . render($content['field_legend_delay']) . '</div>'?>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="row row-centered microstart-font-size" style="padding-bottom:10px;">
<div class="col-xs-11 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;"><div class="panel panel-default"><div data-mh="data-mh-1" class="panel-body">
<div><?php print render($content['field_cred_capital_delay_text']); ?></div>
<div style="color:red;"><?php print ($credit_general_info['cred_term_amount'] * $credit_general_info['cred_qty_term_delayed']) ?>  €</div>
</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col -->
<div class="col-xs-11 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;"><div class="panel panel-default"><div data-mh="data-mh-1" class="panel-body">
<div><?php print render($content['field_cred_next_term_date_text']); ?></div>
<?php
$first_date = DateTime::createFromFormat('d/m/Y', $credit_general_info['cred_next_term_date']);
$current_date = new DateTime('NOW');
$new_date = $first_date->format("d");
if ($first_date->format("d") > $current_date->format("d")) {
  $new_date .= '/' . $current_date->format("m");
} else {
  $current_date->modify('+1 month');
  $new_date .= '/' . $current_date->format("m");
}
$new_date .= '/' . $current_date->format("Y");
print '<div><b>' . $new_date . '</b></div>';
?>
<div style="color:green;"><?php print $credit_general_info['cred_term_amount']; ?>   €</div>
</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col -->
<div class="col-xs-11 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;"><div class="panel panel-default"><div data-mh="data-mh-1" class="panel-body">
<div><?php print render($content['field_cred_remaining_cost_text']); ?></div>
<div class="color-primary"><?php print ($credit_general_info['cred_initial_cost'] - $credit_general_info['cred_capital_reimbursed']); ?> €</div>
</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col -->
</div><!-- /.row -->

<div class="row row-centered visible-md-block visible-lg-block" style="padding-bottom:10px;">
<div class="col-xs-12 col-sm-3 col-centered col-top">
<?php print '<div class="microstart-button microstart-button-secondary microstart-button-font-size"><a href="/credit-management-payement?cred_ref=' . $credit_general_info['cred_ref'] . '">'; ?>
<div class="panel panel-default center-block button-sharp background-color-secondary">
<div class="panel-body" style="padding:0;">
<div style="display:table;width:100%;">
<?php $content['field_image_pay'][0]['#item']['attributes']['class'][] = "img-circle"; ?>
<?php $content['field_image_pay'][0]['#item']['attributes']['style'][] = "width:100px;heigth:100px;"; ?>
<div style="display:inline-block;vertical-align:middle;display:table-cell;"><?php print render($content['field_image_pay']); ?></div>
<div style="display:inline-block;vertical-align:middle;display:table-cell;"><?php print render($content['field_link_pay']); ?></div>
</div>
</div><!-- /.panel-body -->
</div><!-- /.panel -->
</a></div>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="row row-centered visible-xs-block visible-sm-block">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-centered-centered" style="font-size:200%;">
<?php print '<div class="microstart-button-secondary" style="padding-bottom:10px;"><a href="/credit-management-payement?cred_ref=' . $credit_general_info['cred_ref'] . '">'; ?>
<?php print '<div class="panel panel-default button-sharp" style="background-color:#FF9919;"><div class="panel-body">' . render($content['field_link_pay']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="row row-centered" style="padding-bottom:10px;">
<div class="col-xs-12 col-sm-6 col-centered" style="padding-bottom:10px;">
<table class="table table-striped">
<tr>
<th><?php print render($content['field_cred_amount_text']); ?></th>
<td><?php print $credit_general_info['cred_amount']; ?> €</td>
</tr>
<tr>
<th><?php print render($content['field_cred_qty_term_remaining']); ?></th>
<td><?php print $credit_general_info['cred_qty_term']; ?></td>
</tr>
<tr>
<th><?php print render($content['field_cred_capital_reimbursed']); ?></th>
<td><?php print $credit_general_info['cred_residual_reimbursed']; ?> €</td>
</tr>
<tr>
<th><?php print render($content['field_cred_term_amount_text']); ?></th>
<td><?php print $credit_general_info['cred_term_amount']; ?> €</td>
</tr>
<tr>
<th><?php print render($content['field_cred_disbursement_date']); ?></th>
<td><?php print ($credit_general_info['cred_disbursement_date']); ?></td>
</tr>
<tr>
<th><?php print render($content['field_cred_status_text']); ?></th>
<td><?php print $credit_general_info['cred_status']; ?></td>
</tr>
</table>
</div><!-- /.col -->
<!-- <div class="col-xs-12 col-sm-5 col-centered col-top"> -->
<div class="row row-centered">
<div class="col-xs-12">Date du dernier réglement est au <?php print $credit_count[(count($credit_timetable_initial) -1)]['transfert_date']; ?></div><!-- /.col -->
<div class="col-xs-12 color-secondary"><?php print render($content ['field_warning_payement_refused']); ?></div><!-- /.col -->
</div><!-- /.row -->
<!-- </div><!-- /.col -->

</div><!-- /.row -->

<div class="row row-centered">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-centered-centered">
<?php print '<div class="microstart-button" style="padding-bottom:10px;"><a href="/credit-management/report/' . $credit_general_info['cred_ref'] . '">'; ?>
<?php print '<div class="panel panel-default button-sharp microstart-button-font-size" style="background-color:#3B5998;"><div class="panel-body">' . render($content['field_nova_report_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->
</div><!-- /.row -->

</div><!-- /.panel-body --> </div><!-- /.panel --></div><!-- /.col --></div><!-- /.row -->

  </div><!-- /.col --></div><!-- /.panel-body --></div><!-- /.panel -->
  </div><!-- /.ct_c_credit_management -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
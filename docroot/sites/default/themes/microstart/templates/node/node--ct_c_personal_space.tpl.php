<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_greetings_1_title']);
    hide($content['field_greetings_2_title']);
    hide($content['field_personal_space_text']);
    hide($content['field_mytrainings']);
    hide($content['field_mycredits']);
    hide($content['field_mycoachingfeedback']);
    hide($content['field_mywiki']);
    hide($content['field_coaching']);
    hide($content['field_myvolunteers']);
    hide($content['field_myclients']);
    hide($content['field_mycoachees']);
    hide($content['field_dashboard_partner']);
    hide($content['field_registerprospect']);
    hide($content['field_myprospects']);
    hide($content['field_mymicrostart']);
    hide($content['field_mytrainings_image']);
    hide($content['field_mycredits_image']);
    hide($content['field_mycoachingfeedback_image']);
    hide($content['field_mywiki_image']);
    hide($content['field_coaching_image']);
    hide($content['field_mycoachees_image']);
    hide($content['field_dashboard_partner_image']);
    hide($content['field_registerprospect_image']);
    hide($content['field_myprospects_image']);
    hide($content['field_mymicrostart_image']);
  ?>
  <?php //$user = microstart_saleforce_getUser(null); ?>
  <div class="ct_c_personal_space">
    <div class="row row-centered">

      <div class="col-xs-12 col-sm-11 col-centered microstart-font-size">
      <?php print '<div class="color-primary"><h1><div style="display:inline-block;">' . render($content['field_greetings_1_title']) . '</div> <div style="display:inline-block;">' . $user->name . '</div> <div style="display:inline-block;">' .  render($content['field_greetings_2_title']) . '</div></h1></div>'; ?>
      </div><!-- /.col -->

      <div class="col-xs-12 col-sm-11 col-centered microstart-font-size" style="padding-bottom:10px;">
      <?php print render($content['field_personal_space_text']) ?>
      </div><!-- /.col -->

<?php
//CLIENT - TRAINING
// if (user_is_client() || user_is_volunteer()) {
//   $path = 'training-list';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_mytrainings_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_mytrainings_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_mytrainings_image']) . '</div>';
//     print '<div>' . render($content['field_mytrainings']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//CLIENT - CREDIT
// if (user_is_client()) {
//   $path = 'credit-management-list';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_mycredits_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_mycredits_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_mycredits_image']) . '</div>';
//     print '<div>' . render($content['field_mycredits']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//VOLUNTEER - ADVISOR - WIKI
if (user_is_volunteer() || user_is_advisor()) {
  $path = 'wiki';
  if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
    print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
    print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
    $content['field_mywiki_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
    $content['field_mywiki_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
    print '<div style="padding-bottom:10px;">' . render($content['field_mywiki_image']) . '</div>';
    print '<div>' . render($content['field_mywiki'])  . '</div>';
    print '</a></div>';
    print '</div><!-- /.col -->';
  }
}

//ADVISOR - COACHING
// if (user_is_advisor()) {
//   $path = 'coaching-management';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<a href="/' . $path . '">';
//     $content['field_coaching_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_coaching_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_coaching_image']) . '</div>';
//     print '</a>';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">' . render($content['field_coaching']) . '</a></div>';
//     $path = 'volunteer-list';
//     if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//       print '<div class="microstart-button-personal-space"><a href="/' . $path . '">' . render($content['field_myvolunteers']) . '</a></div>';
//     }
//     $path = 'client-list';
//     if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//       print '<div class="microstart-button-personal-space"><a href="/' . $path . '">' . render($content['field_myclients']) . '</a></div>';
//     }
//     print '</div><!-- /.col -->';
//   }
// }

//VOLUNTEER - VOLUNTEER
// if (user_is_volunteer()) {
//   $path = 'volunteer-list';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_coaching_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_coaching_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_coaching_image']) . '</div>';
//     print '<div>' . render($content['field_myvolunteers']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//VOLUNTEER - COACHEES
// if (user_is_volunteer()) {
//   $path = 'coachee-list';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_mycoachees_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_mycoachees_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_mycoachees_image']) . '</div>';
//     print '<div>' . render($content['field_mycoachees']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//PARTNER - DASHBOARD
if (user_is_partner()) {
  global $language;
  switch ($language->language) {
  case 'fr':
    $path = 'node/121';
    break;
  case 'nl':
    $path = 'node/130';
    break;
  default:
    $path = 'node/121';
    //$path = 'dashboard-partner';
    break;
  }
  if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
    drupal_goto('/' . $path);
// //       print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
// //       print '<div class="microstart-button-personal-space"><a href="/dashboard-partner">';
// //       $content['field_dashboard_partner_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
// //       $content['field_dashboard_partner_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
// //       print '<div style="padding-bottom:10px;">' . render($content['field_dashboard_partner_image']) . '</div>';
// //       print '<div>' . render($content['field_dashboard_partner']) . '</div>';
// //       print '</a></div>';
// //       print '</div><!-- /.col -->';
  }
}

//PARTNER - REGISTER PROSPECT
// if (user_is_partner()) {
//   $path = 'eligibility-partner';
//   //$path = '/node/42';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_registerprospect_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_registerprospect_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_registerprospect_image']) . '</div>';
//     print '<div>' . render($content['field_registerprospect']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//PARTNER - PROSPECTS
// if (user_is_partner()) {
//   $path = 'prospect-list';
//   if (drupal_lookup_path('source', $path) || drupal_valid_path($path)) {
//     print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
//     print '<div class="microstart-button-personal-space"><a href="/' . $path . '">';
//     $content['field_myprospects_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
//     $content['field_myprospects_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
//     print '<div style="padding-bottom:10px;">' . render($content['field_myprospects_image']) . '</div>';
//     print '<div>' . render($content['field_myprospects']) . '</div>';
//     print '</a></div>';
//     print '</div><!-- /.col -->';
//   }
// }

//EDITOR
if (user_is_editor()) {
print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
print '<div><a href="/admin/structure/menu">Menu</a></div>';
print '<div><a href="/admin/content">Content</a></div>';
print '<div><a href="/admin/config/regional/translate">Translatable Interface</a></div>';
print '</div><!-- /.col -->';
}

//PROFILE
// print '<div class="col-xs-12 col-sm-3 col-centered-centered col-top" style="padding-bottom:10px;">';
// print '<div class="microstart-button-personal-space"><a href="/user">';
// $content['field_mymicrostart_image'][0]['#item']['attributes']['class'][] = "center-block img-circle";
// $content['field_mymicrostart_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
// print '<div style="padding-bottom:10px;">' . render($content['field_mymicrostart_image']) . '</div>';
// print '<div>' . render($content['field_mymicrostart']) . '</div>';
// print '</a></div>';
// print '</div><!-- /.col -->';
?>
    </div><!-- /.row -->
  </div><!-- /.ct_c_personal_space -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>

<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted): ?>
  <header>
    <?php print render($title_prefix); ?>
    <?php if (!$page && !empty($title)): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <span class="submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </span>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <?php
    // Hide comments, tags, and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    hide($content['field_text_title']);
    hide($content['field_contact_microstart_image']);
    hide($content['field_contact_microstart_text']);
    hide($content['field_training_title_text']);
    hide($content['field_training_localisation_text']);
    hide($content['field_training_seat_text']);
    hide($content['field_training_status_text']);
    hide($content['field_training_description_text']);
    hide($content['field_session_list_text']);
    hide($content['field_session_status_text']);
    hide($content['field_participant_list_text']);
    hide($content['field_session_date_text']);
    hide($content['field_session_hour_text']);
    hide($content['field_participant_status_text']);
    hide($content['field_participant_details_text']);
    hide($content['field_participant_name_text']);
    hide($content['field_participant_first_name_tex']);
    hide($content['field_participant_email_text']);
    hide($content['field_participant_phone_text']);
    hide($content['field_subscribe_text']);
    hide($content['field_unsubscribe_text']);
    hide($content['field_anvers_text']);
    hide($content['field_bruxelles_text']);
    hide($content['field_charleroi_text']);
    hide($content['field_gand_text']);
    hide($content['field_liege_text']);
    hide($content['field_tab_inscription']);
    hide($content['field_tab_registered']);
    hide($content['field_no_trainings_text']);
    hide($content['field_availability_all_text']);
    hide($content['field_availability_registered_te']);
    hide($content['field_availability_new_text']);
    hide($content['field_localisation_text']);
    hide($content['field_availability_text']);
    hide($content['field_session_registered_text']) ;
    hide($content['field_session_passed_text']);
    hide($content['field_session_missed_text']);
    hide($content['field_session_cancelled_text']);
    hide($content['field_session_waiting_text']);
    hide($content['field_training_created_text']);
    hide($content['field_training_cancelled_text']);
    hide($content['field_tab_history']);
  ?>
  <?php $user = microstart_saleforce_getUser(null); ?>
  <?php
    if (user_is_client() /*$user['user_role'] == 1*/) {
      $training_list = microstart_saleforce_client_training_list();
      $training_history_list = microstart_saleforce_client_training_list();
      $client = microstart_saleforce_getClient(null);
    } else {
      $training_list = microstart_saleforce_partner_training_list();
    }
  ?>
  <div class="ct_c_training_list">
  <div class="panel panel-default panel-microstart-back center-block">
  <div class="panel-body">

<div class="row row-centered">

<div class="col-xs-11 col-centered microstart-font-size">
<div class="color-primary"><h1><?php print render($content['field_text_title']) ?></h1></div>
</div><!-- /.col -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-top" style="padding-bottom:10px;">
<?php $content['field_contact_microstart_image'][0]['#item']['attributes']['class'][] = "center-block img-circle"; ?>
<?php $content['field_contact_microstart_image'][0]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;"; ?>
<?php print '<div style="padding-bottom:10px;">' . render($content['field_contact_microstart_image']) . '</div>'; ?>
<?php print '<div class="microstart-button" style="padding-bottom:10px;"><a href="http://microstart.be/fr/contact">'; ?>
<?php print '<div class="panel panel-default button-sharp microstart-button-font-size background-color-primary"><div class="panel-body">' . render($content['field_contact_microstart_text']) . '</div><!-- /.panel-body --></div><!-- /.panel -->'; ?>
<?php print '</a></div>'; ?>
</div><!-- /.col -->

<!-- BLOCK CENTER -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
<!-- BLOCK CENTER -->

<div style="display:inline-block;float:left;">
<?php if (user_is_client()): ?>
<?php print '<div class="microstart-tab-training-list">'; ?>
<?php print '<ul class="nav nav-tabs" style="margin-bottom:0;">'; ?>
<?php print '<li id="filter-new" class="active"><a data-target="#" data-toggle="pill" onclick="modifyFilter_new();">' . render($content['field_tab_inscription']) . '</a></li>'; ?>
<?php print '<li id="filter-registered"><a data-target="#" data-toggle="pill" onclick="modifyFilter_registered();">' . render($content['field_tab_registered']) . '</a></li>'; ?>
<?php print '<li id="filter-history"><a data-target="#" data-toggle="pill" onclick="modifyFilter_history();">' . render($content['field_tab_history']) . '</a></li>'; ?>
<?php print '</ul>'; ?>
<?php print '</div>'; ?>
<?php endif; ?>
</div>

<div id="filter-localisation-block" style="display:inline-block;float:right;">
<div style="display:inline-block;"><?php print render($content['field_localisation_text']); ?></div>
<div style="display:inline-block;"><select id="filter-localisation" class="form-control input-sm" onchange="modifyFilter_localisation()">
<option value="0"><?php print render($content['field_availability_all_text']); ?></option>
<option value="1" <?php if (isset($client) && ($client['agency'] == 1)): print ' selected'; endif;?>><?php print render($content['field_anvers_text']); ?></option>
<option value="2" <?php if (isset($client) && ($client['agency'] == 2)): print ' selected'; endif;?>><?php print render($content['field_bruxelles_text']); ?></option>
<option value="3" <?php if (isset($client) && ($client['agency'] == 3)): print ' selected'; endif;?>><?php print render($content['field_charleroi_text']); ?></option>
<option value="4" <?php if (isset($client) && ($client['agency'] == 4)): print ' selected'; endif;?>><?php print render($content['field_gand_text']); ?></option>
<option value="5" <?php if (isset($client) && ($client['agency'] == 5)): print ' selected'; endif;?>><?php print render($content['field_liege_text']); ?></option>
</select></div>
</div>

<?php // if (count($training_list) <= 0): ?>
<?php print '<div id="no-trainings" class="row row-centered" style="padding-bottom:10px;"><div class="col-xs-12 col-centered-centered microstart-font-size" style="font-weight:bold;"><div class="panel panel-default center-block"><div class="panel-body">' ?>
<?php print render($content['field_no_trainings_text']) ?>
<?php print '</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col --></div><!-- /.row -->' ?>
<?php // endif; ?>
<?php foreach ( $training_list as $training): ?>
<?php
$user_is_registered = false;
if (user_is_client()/*$user['user_role'] == 1*/ && isset($training['session_list'][0]['participant_list']) && (count($training['session_list'][0]['participant_list']) > 0)) {
  $i = 0;
  while (($i < count($training['session_list'][0]['participant_list'])) && ($user_is_registered == false)) {
    if ($user['user_id'] == $training['session_list'][0]['participant_list'][$i]['session_participant_id']) {
      $user_is_registered = true;
    }
    $i++;
  }
}
?>
<?php print '<div id="printable-' . $training['training_id'] . '">'; ?>
<?php print '<div class="row row-centered training training-localisation-' . $training['training_localisation'] ?>
<?php if ($user_is_registered): print ' training-registered'; else: print ' training-new'; endif; ?>
<?php print '" style="padding-bottom:10px;">' ?>
<?php print '<div class="col-xs-12 col-centered">'; ?>

<?php print '<div class="panel panel-default center-block">'; ?>
<?php print '<div class="panel-body">'; ?>


<?php print '<div class="row row-centered">'; ?>
<?php print '<div class="col-xs-12 col-sm-5 col-centered no-padding"><table class="table table-condensed"><tr><th>'  . render($content['field_training_title_text']) .  '</th></tr><tr><td>' . $training['training_title']  . '</td></tr></table></div><!-- /.col -->'; ?>
<?php
switch ($training['training_localisation']) {
    case 1:
        $localisation = render($content['field_anvers_text']);
        break;
    case 2:
        $localisation = render($content['field_bruxelles_text']);
        break;
    case 3:
        $localisation = render($content['field_charleroi_text']);
        break;
    case 4:
        $localisation = render($content['field_gand_text']);
        break;
    case 5:
        $localisation = render($content['field_liege_text']);
        break;
    default:
        $localisation = t('Unknow');
  }
?>
<?php print '<div class="col-xs-12 col-sm-2 col-centered no-padding"><table class="table table-condensed"><tr><th>'  . render($content['field_training_localisation_text']) .  '</th></tr><tr><td>' . $localisation . '</td></tr></table></div><!-- /.col -->'; ?>
<?php print '<div class="col-xs-12 col-sm-2 col-centered no-padding"><table class="table table-condensed"><tr><th>'  . render($content['field_training_seat_text']) .  '</th></tr><tr><td>' . $training['training_seat_available'] . '/' . $training['training_seat_max']  . '</td></tr></table></div><!-- /.col -->'; ?>
<?php print '<div class="col-xs-12 col-sm-2 col-centered no-padding"><table class="table table-condensed"><tr><th>'  . render($content['field_training_status_text']) .  '</th></tr><tr><td>'; ?>
<?php if ($training['training_status'] == 1): print  render($content['field_training_created_text']); else: print  render($content['field_training_cancelled_text']);  endif; ?>
<?php print '</td></tr></table></div><!-- /.col -->'; ?>
<?php print '</div><!-- /.row -->' ?>

<?php print '<div id="' . $training['training_id'] . '" style="display:none">'; ?>

<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered"><table class="table table-condensed"><tr><th>'  . render($content['field_training_description_text']) .  '</th></tr><tr><td>' . $training['training_desc'] . '</td></tr></table></div><!-- /.col --></div><!-- /.row -->'; ?>

<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered"><table class="table table-condensed"><tr><th>'  . render($content['field_session_list_text']) .  '</th></tr></table></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered"><div class="table-responsive"><table class="table table-condensed">'; ?>
<?php print '<tr>' ?>
<?php print '<th>' . render($content['field_session_date_text']) . '</th>'; ?>
<?php print '<th>' . render($content['field_session_hour_text']) . '</th>'; ?>
<?php print '<th>' . render($content['field_session_status_text']) . '</th>'; ?>
<?php if (user_is_client()) /*$user['user_role'] == 1*/ : print '<th>' . render($content['field_participant_status_text']) . '</th>'; endif; ?>
<?php print '</tr>' ?>
<?php if (isset($training['session_list'])): ?>
<?php for ($i = 0; $i < count($training['session_list']); $i++): ?>
<?php print '<tr>' ?>
<?php print '<td>' . $training['session_list'][$i]['session_date'] . '</td>'; ?>
<?php print '<td>' . $training['session_list'][$i]['session_hour'] . '</td>'; ?>
<?php print '<td>'; ?>
<?php if ($training['session_list'][$i]['session_status'] == 1): print render($content['field_training_created_text']); else: print render($content['field_training_cancelled_text']); endif; ?>
<?php print '</td>'; ?>
<?php
if (user_is_client() /*$user['user_role'] == 1*/) {
  if ((!$user_is_registered) || (!isset($training['session_list'][$i]['participant_list']) || (count($training['session_list'][$i]['participant_list']) <= 0))) {
    $status = '/';
  } else {
    $j = 0;
    $found_user_status = false;
    while (($j < count($training['session_list'][$i]['participant_list'])) && ($found_user_status == false)) {
      if ($user['user_id'] == $training['session_list'][$i]['participant_list'][$j]['session_participant_id']) {
        $found_user_status = true;
        switch ($training['session_list'][$i]['participant_list'][$j]['session_participant_status']) {
            case 1:
                $status = render($content['field_session_registered_text']) ;
                break;
            case 2:
                $status = render($content['field_session_passed_text']);
                break;
            case 3:
                $status = render($content['field_session_missed_text']);
                break;
            case 4:
                $status = render($content['field_training_cancelled_text']);
                break;
            case 5:
                $status = render($content['field_session_waiting_text']);
                break;
            default:
                $status = t('Unknow');
        }
      }
      $j++;
    }
  }
  print '<td>' . $status . '</td>';
}
?>
<?php print '</tr>'?>
<?php endfor; ?>
<?php print '</table></div><!-- /.table-responsive --></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php endif; ?>

<?php if (user_is_volunteer()) /*$user['user_role'] == 2*/: ?>
<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered"><table class="table table-condensed"><tr><th>' . render($content['field_participant_list_text']) . '</th></tr></table></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered"><div class="table-responsive"><table class="table table-condensed">'; ?>
<?php print '<tr>'; ?>
<?php print '<th>' . render($content['field_participant_details_text']) . '</th>'; ?>
<?php print '<th>' . render($content['field_participant_name_text']) . '</th>'; ?>
<?php print '<th>' . render($content['field_participant_first_name_tex']) . '</th>'; ?>
<?php print '<th>' . render($content['field_participant_email_text']) . '</th>'; ?>
<?php print '<th>' . render($content['field_participant_phone_text']) . '</th>'; ?>
<?php print '</tr>'; ?>
<?php if (isset($training['session_list'][0]['participant_list'])): ?>
<?php foreach ($training['session_list'][0]['participant_list'] as $participant): ?>
<?php $user =  microstart_saleforce_getUser($participant['session_participant_id']); ?>
<?php print '<tr>' ?>
<?php print '<td><button type="submit" class="btn btn-primary btn-xs button-sharp background-color-primary" onclick="window.location.href=\'/client-details?client_id='. $participant['session_participant_id'] . '\'">Details</button></td>'; ?>
<?php print '<td>' . $user['user_last_name'] . '</td>'; ?>
<?php print '<td>' . $user['user_first_name'] . '</td>'; ?>
<?php print '<td>' . $user['user_email'] . '</td>'; ?>
<?php print '<td>' . $user['user_phone'] . '</td>'; ?>
<?php print '</tr>'; ?>
<?php endforeach; ?>
<?php endif; ?>
<?php print '</table></div><!-- /.table-responsive --></div><!-- /.col --></div><!-- /.row -->'; ?>
<?php endif; ?>

<?php print '</div><!-- /.training_id -->'; ?>

<?php print '<div class="row row-centered"><div class="col-xs-12 col-sm-11 col-centered">'; ?>
<?php print '<span><button type="submit" class="btn btn-primary button-sharp background-color-primary" onclick="showTrainingDetails(\'' . $training['training_id'] . '\')">' . render($content['field_participant_details_text']) . '</button></span> '; ?>
<?php if (user_is_client()) /*$user['user_role'] == 1*/: ?>
<?php print '<span><button class="btn btn-warning button-sharp background-color-secondary" onclick="window.location.href=\'/training-management/'; ?>
<?php if ($user_is_registered): print 'unregister/' . $training['training_id'] . '\'"'; else: print 'register/' . $training['training_id'] . '\'"'; endif; ?>
<?php print '>'; ?>
<?php if ($user_is_registered): print render($content['field_unsubscribe_text']); else: print render($content['field_subscribe_text']); endif; ?>
<?php print '</button></span>'; ?>
<?php endif;?>
<?php $image = file_create_url('public://logo/printer.png'); ?>
<?php //print '<a href="" onclick="printTraining(\'printable-' . $training['training_id'] . '\');">'; ?>
<?php //print '<img src="' . $image . '" style="padding-left:5px;width:30px;height:25px;">'; ?>
<?php //print '</a>'; ?>
<?php print '</div><!-- /.col --></div><!-- /.row -->'; ?>

</div><!-- /.panel-body -->
</div><!-- /.panel -->
</div><!-- /.col -->
</div><!-- /.row -->
<?php print '</div><!-- /.printable -->'; ?>
<?php endforeach; ?>

<?php if (user_is_client()): ?>
<?php print '<div id="history" class="row row-centered" style="padding-bottom:10px;"><div class="col-xs-12 col-centered"><div class="panel panel-default center-block"><div class="panel-body">' ?>
<?php print '<div class="row row-centered">'; ?>
<?php print '<div class="col-xs-12"><table class="table table-condensed" style="margin:0;"><tr><th>'  . render($content['field_training_title_text']) .  '</th></tr><tr><td></td></tr></table></div><!-- /.col -->'; ?>
<?php print '</div><!-- /.row -->' ?>
<?php
if (count($training_history_list) == 0) {
  print '<div>/</div>';
} else {
  foreach ($training_history_list as $training_history){
    print '<div>'  . $training_history['training_title'] . '</div>';
  }
}
 ?>
<?php print '</div><!-- /.panel-body --></div><!-- /.panel --></div><!-- /.col --></div><!-- /.row -->' ?>
<?php endif;?>

<!-- BLOCK CENTER -->
</div><!-- /.col -->
<!-- BLOCK CENTER -->

<?php
$nid = node_view(node_load(68));
$qty=0;
foreach ($nid['field_client_image'] as $key => $value) {
  if (is_numeric($key)) {
    $qty++;
  }
}
$i = 0;
if ($i < $qty) {
  $key = rand  (0, $qty-1);
  $nid['field_client_image'][$key]['#item']['attributes']['class'][] = "center-block img-circle";
  $nid['field_client_image'][$key]['#item']['attributes']['style'][] = "width:220px;heigth:220px;border: 1px solid #0066CC;";
}
?>
<div class="col-xs-12 col-sm-3 col-top col-centered-centered">
<?php print '<div style="padding-bottom:10px;">' . render($nid['field_client_image'][$key]) . '</div>'; ?>
</div><!-- /.col -->


</div><!-- /.row -->

  </div><!-- /.panel-body -->
  </div><!-- /.panel -->
  </div><!-- /.ct_c_training_list -->
  <?php print render($content); ?>
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
  </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?>
</article>
<script type="text/javascript">
function showTrainingDetails(training_id) {
  (function ($) {
    if ($('#' + training_id).is(':hidden')) {
      $('#' + training_id).show();
    } else {
      $('#' + training_id).hide();
    }
  })(jQuery);
}
function modifyFilter_registered() {
  (function ($) {
    $('.training').show();
    $('.training-new').hide();
    $('#filter-localisation-block').show();
    modifyFilter_localisation_only();
    $('#history').hide();
    modifyFilter_no_trainings();
  })(jQuery);
}
function modifyFilter_new() {
  (function ($) {
    $('.training').show();
    $('.training-registered').hide();
    $('#filter-localisation-block').show();
    modifyFilter_localisation_only();
    $('#history').hide();
    modifyFilter_no_trainings();
  })(jQuery);
}
function modifyFilter_history() {
  (function ($) {
    $('.training').hide();
    $("#no-trainings").hide();
    $('#filter-localisation-block').hide();
    $('#history').show();
  })(jQuery);
}
function modifyFilter_no_trainings() {
  (function ($) {
    if (($('.training-registered:visible').length == 0) && ($('.training-new:visible').length == 0)) {
      $("#no-trainings").show();
    } else {
      $("#no-trainings").hide();
    }
  })(jQuery);
}
function modifyFilter_localisation() {
  (function ($) {
    $('.training').show();
    if ($( '#filter-registered' ).hasClass( 'active' )) {
      $('.training-new').hide();
    } else if ($( '#filter-new' ).hasClass( "active" )) {
      $('.training-registered').hide();
    }
    modifyFilter_localisation_only();
    modifyFilter_no_trainings()
  })(jQuery);
}
function modifyFilter_localisation_only() {
  (function ($) {
    switch ($('#filter-localisation').val()) {
    case '1':
      $('.training-localisation-2').hide();
      $('.training-localisation-3').hide();
      $('.training-localisation-4').hide();
      $('.training-localisation-5').hide();
      break;
    case '2':
      $('.training-localisation-1').hide();
      $('.training-localisation-3').hide();
      $('.training-localisation-4').hide();
      $('.training-localisation-5').hide();
      break;
    case '3':
      $('.training-localisation-1').hide();
      $('.training-localisation-2').hide();
      $('.training-localisation-4').hide();
      $('.training-localisation-5').hide();
      break;
    case '4':
      $('.training-localisation-1').hide();
      $('.training-localisation-2').hide();
      $('.training-localisation-3').hide();
      $('.training-localisation-5').hide();
      break;
    case '5':
      $('.training-localisation-1').hide();
      $('.training-localisation-2').hide();
      $('.training-localisation-3').hide();
      $('.training-localisation-4').hide();
      break;
    }
  })(jQuery);
}
function printTraining(training_id) {
  (function ($) {
    $('#' + training_id).show();
    window.print();
  })(jQuery);
}
(function($) {
  $(document).ready(function() {
    modifyFilter_new();
  });
})(jQuery);
</script>

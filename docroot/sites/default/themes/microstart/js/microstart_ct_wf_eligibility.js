/**
 * @file microstart_ct_wf_eligibility.js
 * 
 */


(function($) {
  $(document).ready(function() {
    /*
     * Hide redirection pages
     * 
     */
    hide_all_redirection_pages();
    function hide_all_redirection_pages() {
      $("#node-noCreate").hide();
      $("#node-more6month").hide();
      $("#node-more50000EUR").hide();
      $("#node-earningNotBelgium").hide();
      $("button[value='Next step']").removeAttr("disabled");
    }
    /*
     * Add the function hide/show to eligibility questions
     * 
     */
    $("input[name='submitted[is_degree_in_belgium]']:radio").click(function () {
      $("input:radio[name='submitted[license_basic_management]']").each(function () {
        $(this).removeAttr("checked");
      });
    });
    
    $("input[name='submitted[is_funding_to_create_company]']:radio").click(function () {
      hide_all_redirection_pages();
      if ($("#edit-submitted-is-funding-to-create-company-1").is(":checked")) {
        $("#node-noCreate").show();
        $("button[value='Next step']").attr("disabled","disabled");
      }
      $("#edit-submitted-is-in-activity-1").removeAttr("checked");
      $("#edit-submitted-is-in-activity-2").removeAttr("checked");
    });
    
    $("input[name='submitted[is_in_activity]']:radio").click(function () {
      hide_all_redirection_pages();
      $("#edit-submitted-is-starting-in-less-than-6-month-1").removeAttr("checked");
      $("#edit-submitted-is-starting-in-less-than-6-month-2").removeAttr("checked");
      $("#edit-submitted-is-activiy-provides-earnings-belgium-1").removeAttr("checked");
      $("#edit-submitted-is-activiy-provides-earnings-belgium-2").removeAttr("checked");
    });
    
    $("input[name='submitted[is_starting_in_less_than_6_month]']:radio").click(function () {
      hide_all_redirection_pages();
      if ($("#edit-submitted-is-starting-in-less-than-6-month-1").is(":checked")) {
        $("#node-more6month").show();
        $("button[value='Next step']").attr("disabled","disabled");
      }
      $("#edit-submitted-is-activiy-provides-earnings-belgium-1").removeAttr("checked");
      $("#edit-submitted-is-activiy-provides-earnings-belgium-2").removeAttr("checked");
    });
    
    $("input[name='submitted[is_activiy_provides_earnings_belgium]']:radio").click(function () {
      hide_all_redirection_pages();
      if ($("#edit-submitted-is-activiy-provides-earnings-belgium-1").is(":checked")) {
        $("#node-earningNotBelgium").show();
        $("button[value='Next step']").attr("disabled","disabled");
      }
      $("#edit-submitted-is-need-less-than-50000-1").removeAttr("checked");
      $("#edit-submitted-is-need-less-than-50000-2").removeAttr("checked"); 
    });
    

    $("input[name='submitted[is_need_less_than_50000]']:radio").click(function () {
      hide_all_redirection_pages();
      if ($("#edit-submitted-is-need-less-than-50000-1").is(":checked")) {
        $("#node-more50000EUR").show();
        $("button[value='Next step']").attr("disabled","disabled");
      }
    });
    
  });
})(jQuery);
<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

/**
 * Implements hook_theme().
 * Custom user profile edit page.
 */
function microstart_theme() {
  return array(
    // The form ID.
    'user_login' => array(
      // Forms always take the form argument.
      'variables' => array('form' => NULL),
      'template' => 'templates/node/user_login',
    ),
    'user_register_form' => array(
      'variables' => array('form' => NULL),
      'template' => 'templates/system/user-register',
    ),
    'user_pass' => array(
      'variables' => array('form' => NULL),
      'template' => 'templates/system/user-pass',
    ),
    'user_profile_form' => array(
      'variables' => array('form' => NULL),
      'template' => 'templates/system/user-profile-edit',
    ),
  );
}

// function your_themename_theme() {
//   $items = array();
//   // create custom user-login.tpl.php
//   $items['user_login'] = array(
//   'render element' => 'form',
//   'path' => drupal_get_path('theme', 'your_themename') . '/templates',
//   'template' => 'user-login',
//   'preprocess functions' => array(
//   'your_themename_preprocess_user_login'
//   ),
//  );
// return $items;
// }

/**
 * Implements hook_preprocess_page().
 * Override pages templates.
 */
function microstart_preprocess_page(&$variables) {
  if (isset ( $variables ['node']->type )) {
    $variables ['theme_hook_suggestions'] [] = 'page__' . $variables ['node']->type;
  }
}

/**
 * Implements hook_preprocess_webform-form().
 * Override block templates
 */
function microstart_preprocess_webform_form(&$variables) {
  if (isset ( $variables ['form'] ['#node']->type )) {
    $variables ['theme_hook_suggestions'] [] = 'form__' . $variables ['form'] ['#node']->type;
  }
}

/**
 * Implements hook_form_alter().
 * Remove default redirect to set the one defined in Action/Trigger.
 */
function microstart_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_login_block') {
    unset ( $form ['#action'] );
  }
}

/**
 * Implementation of hook_menu_alter().
 * Remember to clear the menu cache after adding/editing this function.
 */

/**
 * Implements hook_menu_alter().
 * Hide menu 'Navigation' for 'volunteer' or 'advisor' role.
 * Purpose is to display instead the  'Wiki' menu (add Content) only on 'Wiki Page' content type.
 *
 */
// function microstart_menu_alter(&$items) {
//   print "<h1>bonjour</h1>";
//   unset($items['navigation']);
// }

// function MODULENAME_menu_alter(&$items) {
//   // Removing certain local navigation tabs that are either undesired or need to be custom relocated.

//   // Set these tabs to MENU_CALLBACK, so they still register the path, but just don't show the tab:
//   $items['node/%node/track']['type'] = MENU_CALLBACK;
//   $items['user/%user/track']['type'] = MENU_CALLBACK;
//   $items['search/user/%menu_tail']['type'] = MENU_CALLBACK;

//   // Fully unset these tabs and their paths, don't want them at all. This breaks the path as well:
//   unset($items['user/%user/example']);
// }

/**
 * Implements hook_preprocess().
 */
function microstart_preprocess_node(&$variables) {
      switch($variables['type']) {
      case 'ct_c_credit_management':
        drupal_add_js ( path_to_theme () . '/js/jquery.matchHeight.js' );
        break;
      case 'ct_c_volunteer_list':
      case 'ct_c_coaching_management':
        drupal_add_js ( path_to_theme () . '/js/jquery.googlemap.js' );
        //google api key added in template itself to an issue when adding parameters
        break;
    }
}
<?php

/**
 * @file
 * A block module that manage the rules on the portal.
 */

/**
 * Rules declaration (Used for the wiki mailings).
 *
 */
function microstart_rules_rules_action_info() {
  return array(
//     'mail_to_users_of_role_wiki' => array(
//       'label' => t('Send mail to all users of a role based on their wiki profile'),
//       'group' => t('System'),
//       'parameter' => array(
//         'roles' => array(
//           'type' => 'list<integer>',
//           'label' => t('Roles'),
//           'options list' => 'entity_metadata_user_roles',
//           'description' => t('Select the roles whose users should receive the mail.'),
//         ),
//         'subject' => array(
//           'type' => 'text',
//           'label' => t('Subject'),
//           'description' => t("The mail's subject."),
//         ),
//         'message' => array(
//           'type' => 'text',
//           'label' => t('Message'),
//           'description' => t("The mail's message body."),
//         ),
//         'from' => array(
//           'type' => 'text',
//           'label' => t('From'),
//           'description' => t("The mail's from address. Leave it empty to use the site-wide configured address."),
//           'optional' => TRUE,
//         ),
//       ),
//       'base' => 'rules_action_mail_to_users_of_role_wiki',
//       'access callback' => 'rules_system_integration_access',
//     ),
    'mimemail_to_users_of_role_wiki' => array(
      'label' => t('Send HTML mail to all users of a role based on their wiki profile'),
      'group' => t('System'),
      'parameter' => array(
        'key' => array(
          'type' => 'text',
          'label' => t('Key'),
          'description' => t('A key to identify the e-mail sent.'),
        ),
        'roles' => array(
          'type' => 'list<integer>',
          'label' => t('Roles'),
          'options list' => 'entity_metadata_user_roles',
          'description' => t('Select the roles whose users should receive the mail.'),
        ),
        'active' => array(
          'type' => 'boolean',
          'label' =>('Send to active users'),
          'description' => t('Send mail only to active users.'),
        ),
        'from_name' => array(
          'type' => 'text',
          'label' => t('Sender name'),
          'description' => t("The sender's name. Leave it empty to use the site-wide configured name."),
          'optional' => TRUE,
        ),
        'from_mail' => array(
          'type' => 'text',
          'label' => t('Sender e-mail address'),
          'description' => t("The sender's address. Leave it empty to use the site-wide configured address."),
          'optional' => TRUE,
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject'),
          'description' => t("The mail's subject."),
          'translatable' => TRUE,
        ),
        'body' => array(
          'type' => 'text',
          'label' => t('Body'),
          'description' => t("The mail's message HTML body."),
          'optional' => TRUE,
          'translatable' => TRUE,
        ),
        'plaintext' => array(
          'type' => 'text',
          'label' => t('Plaintext body'),
          'description' => t("The mail's message plaintext body."),
          'optional' => TRUE,
          'translatable' => TRUE,
        ),
        'attachments' => array(
          'type' => 'text',
          'label' => t('Attachments'),
          'description' => t("The mail's attachments, one file per line e.g. \"files/images/mypic.png\" without quotes."),
          'optional' => TRUE,
        ),
        'language_user' => array(
          'type' => 'boolean',
          'label' => t("Send mail in each recipient's language"),
          'description' => t("If checked, the mail message and subject will be sent in each user's preferred language. <strong>You can safely leave the language selector below empty if this option is selected.</strong>"),
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Fixed language'),
          'description' => t('If specified, the fixed language used for getting the mail message and subject.'),
          'options list' => 'entity_metadata_language_list',
          'optional' => TRUE,
          'default value' => LANGUAGE_NONE,
          'default mode' => 'selector',
        ),
      ),
      'base' => 'rules_action_mimemail_to_users_of_role_wiki',
      'access callback' => 'rules_system_integration_access',
    ),
  );
}

/**
 * Action: Send HTML mail to all users of a specific role group(s) based on their wiki profile.
 */
function rules_action_mimemail_to_users_of_role_wiki($key, $roles, $active, $from_name = NULL, $from_mail = NULL, $subject, $body, $plaintext = NULL, $attachments = array(), $use_userlang = FALSE, $langcode= NULL, $settings, RulesState $state, RulesPlugin $element) {
  module_load_include('inc', 'mimemail');

  // Set the sender name and from address.
  if (empty($from_mail)) {
    $from = NULL;
  }
  else {
    $from = array(
      'name' => $from_name,
      'mail' => $from_mail,
    );
    // Create an address string.
    $from = mimemail_address($from);
  }

  $query = db_select('users', 'u');
  $query->fields('u', array('mail', 'language'));

  if ($active) {
    $query->condition('u.status', 1, '=');
  }

  if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
    $query->condition('u.uid', 0, '>');
  }
  else {
    $query->join('users_roles', 'r', 'u.uid = r.uid');
    $query->join('field_data_field_notification_wiki', 'wiki', 'u.uid = wiki.entity_id AND wiki.field_notification_wiki_value = 1');
    //$query->condition('wiki.field_notification_wiki_value', 1, '=');
    $query->condition('r.rid', $roles, 'IN');
    $query->distinct();
  }

  $result = $query->execute();

  $params = array(
    'context' => array(
      'subject' => $subject,
      'body' => $body,
      'action' => $element,
      'state' => $state,
    ),
    'plaintext' => $plaintext,
    'attachments' => $attachments,
  );

  // Create language list before initializing foreach.
  $languages = language_list();

  $message = array('result' => TRUE);
  foreach ($result as $row) {
    // Decide which language to use.
    if (!$use_userlang || empty($row->language) || !isset($languages[$row->language])) {
      $language = isset($languages[$langcode]) ? $languages[$langcode] : language_default();
    }
    else {
      $language = $languages[$row->language];
    }

    $message = drupal_mail('mimemail', $key, $row->mail, $language, $params, $from);
    if (!$message['result']) {
      break;
    }
  }
  if ($message['result']) {
    $role_names = array_intersect_key(user_roles(TRUE), array_flip($roles));
    watchdog('rules', 'Successfully sent HTML email to the role(s) %roles.', array('%roles' => implode(', ', $role_names)));
  }
}


/**
 * Action: Send mail to all users of a specific role group(s) based on their wiki profile.
 *
 */
// function rules_action_mail_to_users_of_role_wiki($roles, $subject, $message, $from = NULL, $settings, RulesState $state, RulesPlugin $element) {
//   $from = !empty($from) ? str_replace(array("\r", "\n"), '', $from) : NULL;

//   // All authenticated users, which is everybody.
//   if (in_array(DRUPAL_AUTHENTICATED_RID, $roles)) {
//     $result = db_query('SELECT mail FROM {users} WHERE uid > 0');
//   }
//   else {
//     $rids = implode(',', $roles);
//     // Avoid sending emails to members of two or more target role groups.
//     //$result = db_query('SELECT DISTINCT u.mail FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN ('. $rids .')');

//     $query = 'SELECT DISTINCT u.mail FROM {users} u';
//     $query .= ' INNER JOIN {users_roles} r ON u.uid = r.uid';
//     $query .= ' INNER JOIN {field_data_field_notification_wiki} wiki ON u.uid = wiki.entity_id AND wiki.field_notification_wiki_value = 1';
//     $query .= ' WHERE r.rid IN ('. $rids .')';
//     $result = db_query($query);
//   }

//   // Now, actually send the mails.
//   $params = array(
//     'subject' => $subject,
//     'message' => $message,
//   );
//   // Set a unique key for this mail.
//   $name = isset($element->root()->name) ? $element->root()->name : 'unnamed';
//   $key = 'rules_action_mail_to_users_of_role_wiki_' . $name . '_' . $element->elementId();  $languages = language_list();

//   $message = array('result' => TRUE);
//   foreach ($result as $row) {
//     $message = drupal_mail('rules', $key, $row->mail, language_default(), $params, $from);
//     // If $message['result'] is FALSE, then it's likely that email sending is
//     // failing at the moment, and we should just abort sending any more. If
//     // however, $mesage['result'] is NULL, then it's likely that a module has
//     // aborted sending this particular email to this particular user, and we
//     // should just keep on sending emails to the other users.
//     // For more information on the result value, see drupal_mail().
//     if ($message['result'] === FALSE) {
//       break;
//     }
//   }
//   if ($message['result'] !== FALSE) {
//     $role_names = array_intersect_key(user_roles(TRUE), array_flip($roles));
//     watchdog('rules', 'Successfully sent email to the role(s) %roles.', array('%roles' => implode(', ', $role_names)));
//   }
// }
Description
-----------
This module provides a field to integrate 
with Skype buttons and Skype URI's. 
The module contains 2 formatters, both configurable.

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your modules directory.

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Modules, and enable "Skype".

2. - Add a field of type "Skype" on your content type
   - Go to admin -> structure -> content types -> your contenttype -> manage display
     and choose to display a Skype button or a Skype URI
   - Add a node and fill out a skype ID in the field you just created
   - Navigate to the node view and admire your skype button/URI
